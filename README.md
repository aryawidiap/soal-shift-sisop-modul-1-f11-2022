# soal-shift-sisop-modul-1-F11-2022

Anggota Kelompok

 - Arya Widia Putra	5025201016
 - Muhammad Rolanov Wowor	5025201017
 - Muhammad Damas Abhirama	5025201271

## Soal 1

1a. Membuat sistem register dan login dimana setiap user yang berhasil daftar akan masuk ke ./users/user.txt 
```
echo  "$username" >> ./users/user.txt
```
```
echo  "$password" >> ./users/user.txt
```
echo $username dan $password akan menampilkan username dan password yang telah diinput user lalu memasukkannya ke path ./users/user.txt

screenshot register berhasil
<br>
![Gambar-1_register_berhasil](/img/1_register_berhasil.png)

username dan password akan tersimpan di path ./users/user.txt
<br>
![Gambar-1_user_register](/img/1_user_register.png)

1b. Disini kami menggunakan -s pada read password agar password yang di input bersifat hidden. Disini kami menyimpan panjang password ke dalam variabel len untuk conditional statement selanjutnya yang bertujuan agar password yang diinput harus berjumlah 8. Conditional statement ini bertujuan agar password yang diinput harus memenuhi kriteria berikut:
a. Minimal 8 karakter
b. Memiliki 1 huruf kapital dan 1 huruf kecil
c. Alphanumeric
d. Tidak boleh sama dengan username

```
read -s -p "Password: " password
len="${#password}"

if  test  $len -ge 8
then
	echo  "$password" | grep -q [0-9]
	if  test  $? -eq 0
	then
		echo  "$password" | grep -q [A-Z]
		if  test  $? -eq 0
		then
			echo  "$password" | grep -q [a-z]
			if  test  $? -eq 0
			then
				if [ "$password"  ==  "$username"]
				then
					echo  "password tidak boleh sama dengan username"
				else
					echo  "$password" >>./users/user.txt
					echo -e "\nPassword berhasil terdaftar"
					fi
			else
				echo  "Password minimal terdiri dari 1 huruf kecil"
			fi
		else
			echo  "Password minimal terdiri dari 1 huruf kapital"
		fi
	else
		echo  "password harus memiliki angka"
	fi
else
	echo  "panjang password minimal 8"
fi
```
1c. Untuk message log, kita menggunakan `>>` untuk memasukkan ke dalam path ./users/user.txt. Agar tanggal dan waktu pada message log sesuai dengan format soal, kami menyimpannya terlebih dahulu pada variabel tanggal dan waktu seperti berikut
```
tanggal="$(date +'%m/%d/%Y')"
waktu="$(date +'%r')"
echo  "$tanggal  $waktu REGISTER: INFO User $username registered succesfully" >> log.txt
```
screenshot pesan pada log.txt
<br>
![Gambar-1_MESSAGE](/img/1_MESSAGE.png)

1d. Setelah user berhasil login, maka user dapat melakukan input `dl N` untuk mendownload gambar sebanyak N, atau `att` untuk menghitung berapa banyak attempt login yang berhasil maupun yang tidak berhasil. 

```
if [ "$command"  =  "dl" ]
then
read N
mkdir "$(date +"%Y-%m-%d")_$username"
folder="$(date +"%Y-%m-%d")_$username"
a=1
while [ $a  -le  $N ]
do
	wget -O ./$folder/PIC_0$a https://loremflickr.com/320/240
	a=$((a +  1))
done
cd  $folder
b=1
while [ $b  -le  $N ]
do
	if [ -f  $folder.zip ]
	then
		unzip -n -P $password  $folder.zip
	fi
	zip -P $password -r $folder PIC_0$b
	b=$((b +  1))
done
elif [ "$command"  =  "att" ]
then
	grep -c "LOGIN" log.txt
fi
```
kendala kami di sini yaitu kami tidak berhasil untuk melakukan unzip jika terdapat file zip dengan format nama yang sama

screenshot user melakukan input dl 5
<br>
![Gambar-1_zip](/img/1_zip.png)

screenshot user melakukan input att
<br>
![Gambar-1_att](/img/1_att.png)


## Soal 2

1. membuat folder dengan nama forensic_log_website
   `mkdir forensic_log_website_daffainfo_log` 

2. awk -F: 'NR>1'{print $3}' untuk menampilkan argument ke 3 dari yaitu jam 00 hingga 12 dari file log_website_daffaindo.log dengan NR>1 yang artinya baris pertama di skip. lalu akan masuk ke uniq -c dimana uniq untuk menyatukan bilangan yang duplikat menjadi 1 dan -c untuk menghitung masing masing dan muncul menjadi argumen pertama yaitu $1. lalu semua $1 dijumlahkan dan print rata rata nya dengan rumus sum/NR dimana NR adalah jumlah barisnya. lalu hasilnya dimasukkan ke file ratarata.txt 
```
awk -F: 'NR>1{print $3}' log_website_daffainfo.log | uniq -c | awk '{sum+=$1} END {printf "Rata-rata serangan adalah sebanyak %d requests per jam\n", sum/NR}' >> ./forensic_log_website_daffainfo_log/ratarata.txt
```
3. awk -F: 'NR>1{print $1}' untuk menampilkan argument ke 1 yaitu IP dengan mengskip baris pertama dari file log_website_daffainfo.log . lalu masuk ke uniq -c untuk menyatukan string yang duplikat menjadi 1 dan -c untuk menghitung jumlah masing masingnya. lalu sort -n untuk menjadikan yang terbesar di bawah. lalu masuk ke tail -n 1 untuk memilih baris yang paling bawah. lalu hasil print akan masuk ke result.txt
```
awk -F: 'NR>1{print $1}' log_website_daffainfo.log | uniq -c | sort -n | tail -n 1 | awk '{ printf "IP yang paling banyak mengakses server adalah: %s sebanyak %s request\n\n", $2, $1}' >> ./forensic_log_website_daffainfo_log/result.txt
```
4. awk -F: 'NR>1{print $9}' untuk menampilkan argumen ke 9 dengan men-skip baris pertama dari file log_website_daffainfo.log. lalu masuk ke grep "curl" untuk mencari kata "grep". lalu masuk ke uniq -c dimana uniq untuk menyatukan data yang kembar dan -c untuk menghitung masing masing. lalu hasil printf akan masuk ke result.txt
```
awk -F: 'NR>1{print $9}' log_website_daffainfo.log | grep "curl" | uniq -c |awk '{ printf "ada %d request yang menggunakan curl sebagai user-agent\n\n", $1}' >> ./forensic_log_website_daffainfo_log/result.txt
```
5. awk -F: 'NR>1{printf "%s %s\n", $1, $3}' untuk menampilkan argumen 1 dan 3, argumen pertama adalah IP, argumen 3 adalah jam (00-12). di sini argumen 2 menjadi jam (00-12). lalu kita menampilkan semuanya yang  argumen 2 nya adalah "02" . setelah itu argumen 1 yaitu IP ditampilkan dan dimasukkan ke result.txt
```
awk -F: 'NR>1{printf "%s %s\n", $1, $3}' log_website_daffainfo.log | awk '($2 =="02") {print}' | awk '{print $1}' >> ./forensic_log_website_daffainfo_log/result.txt
```

Screenshot dari file ratarata.txt
<br>
![Gambar-2_ratarata](/img/2_ratarata.png)

Screenshot dari file result.txt
<br>
![Gambar-2_result](/img/2_result.png)

## Soal 3

3a. Membuat file log dengan nama metrics{YmdHms}.log.
```
    timestamp=$(date '+%Y%m%d%H%M%S')
    mkdir -p /home/"$USER"/log
```
contoh isi file metrics : script dijalankan pada tanggal 6 Maret 2022, pukul 13:31:44 WIB
<br>
![Gambar-3_metricsmonitoring](/img/3_metricsmonitoring.png)

3b. Mencatat semua file metrics yang berjalan setiap menit, sehingga perlu menggunakan cronjob untuk menjalankan script setiap menit
```
add_cron_jobs(){
    crontab -l > cron_jobs
    echo "* * * * * "$(pwd | awk '{print}')"/minute_log.sh" >> cron_jobs
    crontab cron_jobs
}
```
Screenshot hasil running script setiap menit
<br>
![Gambar-3_metricsminute](/img/3_metricsminute.png)

3c. Agregasi file log dalam satuan jam, menggunakan cron job yang menjalankan script setiap jam
```
add_cron_jobs(){
	crontab -l > cron_jobs
    echo "0 * * * * "$(pwd | awk '{print}')"/$0" >> cron_jobs
    crontab cron_jobs
}
```
Kendala yang dialami adalah tidak menampilkan nilai minimum, maximum, dan rata-rata dari tiap metrics. Sehingga hanya menampilkan monitoring ram dan directory size saja yang dijalankan setiap jam.

3d. File log hanya dapat dibaca oleh user pemilik file, menggunakan chmod 600
```
	chmod 600 -- /home/"$USER"/log/"metrics_$timestamp".log
```
Keterangan tambahan :
Mendapatkan hasil angka dari monitoring ram dan directory size menggunakan awk.
```
free -m | awk "/Mem:/" | awk '{printf $2","$3","$4","$5","$6}'  >> /home/"$USER"/log/"metrics_$timestamp".log
free -m | awk "/Swap:/" | awk '{printf $2","$3","$4","}' >> /home/"$USER"/log/"metrics_$timestamp".log
du -sh /home/"$USER" | awk '{ print $2"/,"$1 }' >> /home/"$USER"/log/"metrics_$timestamp".log
```
awk "/Mem:/" untuk mendapatkan angka-angka dari perintah free -m dibaris kedua. dan awk "/Swap:/" untuk mendapatkan angka-angka dari perintah free -m dibaris ketiga. Sedangkan untuk monitoring directory size, argumen perlu ditukar karena pada metrics yang ditampilkan adalah directorynya terlebih dahulu lalu diikuti dengan sizenya.
