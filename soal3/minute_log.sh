#!/bin/bash
monitoring_ram(){
    echo "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" >> /home/"$USER"/log/"metrics_$timestamp".log
    free -m | awk "/Mem:/" | awk '{printf $2","$3","$4","$5","$6}'  >> /home/"$USER"/log/"metrics_$timestamp".log
    free -m | awk "/Swap:/" | awk '{printf $2","$3","$4","}' >> /home/"$USER"/log/"metrics_$timestamp".log
    chmod 600 -- /home/"$USER"/log/"metrics_$timestamp".log
}

monitoring_dirsize(){
    du -sh /home/"$USER" | awk '{ print $2"/,"$1 }' >> /home/"$USER"/log/"metrics_$timestamp".log
    chmod 600 -- /home/"$USER"/log/"metrics_$timestamp".log
}
add_cron_jobs(){
    echo "* * * * * "$(pwd | awk '{print}')"/minute_log.sh" >> cron_jobs
    crontab cron_jobs
}
main (){
    timestamp=$(date '+%Y%m%d%H%M%S')
    mkdir -p /home/"$USER"/log
    monitoring_ram
    monitoring_dirsize
    add_cron_jobs
}
main