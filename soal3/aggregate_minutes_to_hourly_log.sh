#!/bin/bash
monitoring_ram(){
    echo "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" >> /home/"$USER"/log/"metrics_$hourly_timestamp".log
    free -m | awk "/Mem:/" | awk '{printf $2","$3","$4","$5","$6}'  >> /home/"$USER"/log/"metrics_$hourly_timestamp".log
    free -m | awk "/Swap:/" | awk '{printf $2","$3","$4","}' >> /home/"$USER"/log/"metrics_$hourly_timestamp".log
    chmod 600 -- /home/"$USER"/log/"metrics_$hourly_timestamp".log
}

monitoring_dirsize(){
    du -sh /home/"$USER" | awk '{ print $2"/,"$1 }' >> /home/"$USER"/log/"metrics_$hourly_timestamp".log
    chmod 600 -- /home/"$USER"/log/"metrics_$hourly_timestamp".log
}
add_cron_jobs(){
    crontab -l > cron_jobs
    echo "0 * * * * "$(pwd | awk '{print}')"/$0" >> cron_jobs
    crontab cron_jobs
}
main (){
    hourly_timestamp=$(date '+%Y%m%d%H')
    mkdir -p /home/"$USER"/log
    monitoring_ram
    monitoring_dirsize
    add_cron_jobs
}
main
