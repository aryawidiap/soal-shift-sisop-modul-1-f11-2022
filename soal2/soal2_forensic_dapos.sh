#!/bin/bash

mkdir forensic_log_website_daffainfo_log

awk -F: 'NR>1{print $3}' log_website_daffainfo.log | uniq -c | awk '{sum+=$1} END {printf "Rata-rata serangan adalah sebanyak %d requests per jam\n", sum/NR}' >> ./forensic_log_website_daffainfo_log/ratarata.txt

awk -F: 'NR>1{print $1}' log_website_daffainfo.log | uniq -c | sort -n | tail -n 1 | awk '{ printf "IP yang paling banyak mengakses server adalah: %s sebanyak %s request\n\n", $2, $1}' >> ./forensic_log_website_daffainfo_log/result.txt

awk -F: 'NR>1{print $9}' log_website_daffainfo.log | grep "curl" | uniq -c |awk '{ printf "ada %d request yang menggunakan curl sebagai user-agent\n\n", $1}' >> ./forensic_log_website_daffainfo_log/result.txt

awk -F: 'NR>1{printf "%s %s\n", $1, $3}' log_website_daffainfo.log | awk '($2 =="02") {print}' | awk '{print $1}' >> ./forensic_log_website_daffainfo_log/result.txt