#!/bin/bash

read -p "Username: " username
grep -q $username ./users/user.txt
if [ $? -eq 0 ]
then
    echo "Username $username sudah ada!"
    echo "Silahkan Masukkan Username Lain"
    tanggal="$(date +'%m/%d/%Y')"
    waktu="$(date +'%r')"
    echo "$tanggal $waktu REGISTER: ERROR User alredy exists" >> log.txt
else
    echo "$username" >> ./users/user.txt
    tanggal="$(date +'%m/%d/%Y')"
    waktu="$(date +'%r')"
    echo "$tanggal $waktu REGISTER: INFO User $username registered succesfully" >> log.txt
fi

read -s -p "Password: " password
len="${#password}"

if test $len -ge 8
then
    echo "$password" | grep -q [0-9]
    if test $? -eq 0
    then
        echo "$password" | grep -q [A-Z]
        if test $? -eq 0
        then
            echo "$password" | grep -q [a-z]
            if test $? -eq 0
            then
                if [ "$password" == "$username" ]
                then
                    echo "password tidak boleh sama dengan username"
                else
                    echo "$password" >> ./users/user.txt
                    echo -e "\nPassword berhasil terdaftar"
                fi
            else
                echo "Password minimal terdiri dari 1 huruf kecil"
            fi
        else
            echo "Password minimal terdiri dari 1 huruf kapital"
        fi
    else
        echo "password harus memiliki angka"
    fi
else
    echo "panjang password minimal 8"
fi